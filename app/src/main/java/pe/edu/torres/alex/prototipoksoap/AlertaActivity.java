package pe.edu.torres.alex.prototipoksoap;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import pe.edu.torres.alex.prototipoksoap.Adapters.AlertaAdapter;
import pe.edu.torres.alex.prototipoksoap.Controller.AlertaController;
import pe.edu.torres.alex.prototipoksoap.bean.AlertaBean;

public class AlertaActivity extends AppCompatActivity {
    EditText edtCod,edtTit,edtObs;
    AlertaController alertaController = new AlertaController(AlertaActivity.this);
    View linearLayoutView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerta);

        // ENLAZAR OBJECT XML A OBJECT JAVA
        linearLayoutView = findViewById(R.id.MyLinearLayout);
        edtCod = findViewById(R.id.edtCodigo);
        edtTit = findViewById(R.id.edtTitulo);
        edtObs = findViewById(R.id.edtObserv);
        edtCod.setEnabled(false);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null ) {
            edtCod.setText(bundle.getString("correl"));
            edtTit.setText(bundle.getString("titulo"));
            edtObs.setText(bundle.getString("observ"));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_alerta, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        HashMap<String, Object> mapParametros = new HashMap<>();
        if (id == R.id.action_guardar) {
            //TODO - CONSUMO DEL SERVICIO SOAP + PARAMETROS
            mapParametros.put("arg_tipo","A");
            mapParametros.put("arg_titulo",edtTit.getText());
            mapParametros.put("arg_observ",edtObs.getText());
            mapParametros.put("arg_estcod",1);
            mapParametros.put("arg_correl",edtCod.getText());

            new Hilo(mapParametros,1).execute();
        }
        if (id == R.id.action_eliminar) {
            //TODO - CONSUMO DEL SERVICIO SOAP + PARAMETROS
            mapParametros.put("arg_correl",edtCod.getText());

            new Hilo(mapParametros,2).execute();
        }

        Intent x= new Intent(AlertaActivity.this,MainActivity.class);
        startActivity(x);

        return super.onOptionsItemSelected(item);
    }


    //CLASE ASYNCTASK
    public class Hilo extends AsyncTask<String,Integer,JSONObject> {
        HashMap<String, Object> mapParametros;
        int OPERACION;

        //DIALOG
        ProgressDialog dialog = null;

        //CONSTRUCTOR
        public Hilo(HashMap<String, Object> mapParametros, int OPERACION) {
            this.mapParametros = mapParametros;
            this.OPERACION = OPERACION;
        }

        @Override
        protected void onPreExecute() {
            //TODO - MOSTRAR CUADRO DE DIALOGO EMERGENTE
            dialog = new ProgressDialog(AlertaActivity.this);
            dialog.setTitle("Aviso");
            dialog.setMessage("Procesando...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            JSONObject jsonObject= null;
            switch (this.OPERACION){
                case 0:
                    jsonObject = alertaController.insAlerta(this.mapParametros);
                    break;
                case 1:
                    jsonObject = alertaController.updAlerta(this.mapParametros);
                    break;
                case 2:
                    jsonObject = alertaController.delAlerta(this.mapParametros);
                    break;
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            //TODO - CERRAR PROGRESSDIALOG SI ESTA VISIBLE
            if(this.dialog.isShowing()){this.dialog.dismiss();}
            try {
                //TODO - MOSTRAR KEY MESSAGE
                Snackbar.make(linearLayoutView, jsonObject.getString("message") ,Snackbar.LENGTH_LONG).show();
            }catch (Exception e){
                Log.e("ERROR",e.getMessage());
            }
        }


    }


}
