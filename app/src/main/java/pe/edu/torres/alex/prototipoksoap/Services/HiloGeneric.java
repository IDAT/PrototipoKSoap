package pe.edu.torres.alex.prototipoksoap.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.HashMap;



public class HiloGeneric extends AsyncTask<String,Integer,String> {
    private final Context context;
    public final String OPERATION_NAME;
    public final String WSDL_TARGET_NAMESPACE;
    public final String SOAP_ADDRESS;
    public final HashMap<String,String> PARAMETERS;
    CallbackGeneric<String> callback;
    ProgressDialog dialog;

    public HiloGeneric(Context context, String WSDL_TARGET_NAMESPACE, String SOAP_ADDRESS, String OPERATION_NAME, HashMap<String,String> PARAMETERS, CallbackGeneric callback) {
        this.context = context;
        this.WSDL_TARGET_NAMESPACE = WSDL_TARGET_NAMESPACE;
        this.SOAP_ADDRESS = SOAP_ADDRESS;
        this.OPERATION_NAME = OPERATION_NAME;
        this.PARAMETERS = PARAMETERS;
        this.callback=callback;
    }

    @Override
    protected void onPreExecute() {
        this.dialog = new ProgressDialog(context);
        this.dialog.setTitle("");
        this.dialog.setMessage("Cargando...");
        this.dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        this.dialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        SoapClient soapClient = new SoapClient();
        return soapClient.Call(WSDL_TARGET_NAMESPACE,SOAP_ADDRESS,OPERATION_NAME, PARAMETERS);
    }

    @Override
    protected void onPostExecute(String strResponse) {
        if(this.dialog.isShowing()){this.dialog.dismiss();}
        Log.i("idat1", strResponse);
        callback.onSuccess(strResponse);

    }

}