package pe.edu.torres.alex.prototipoksoap.Services;

import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class SoapClient {
    public SoapClient(){
    }

    public String Call(String WSDL_TARGET_NAMESPACE, String SOAP_ADDRESS, String OPERATION_NAME, HashMap<String, String> PARAMETERS)
    {
        String SOAP_ACTION = WSDL_TARGET_NAMESPACE + OPERATION_NAME; //SOAP_ACTION
        HttpTransportSE httpTransport = null;
        SoapPrimitive response=null;

        try{
            //TODO Modelo de Request
            SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME); //SOLICITUD

            //TODO Carga Parametros Iterando el Map
            Iterator it = PARAMETERS.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry e = (Map.Entry)it.next();
                request.addProperty(e.getKey().toString(), e.getValue().toString());

                /* METODO TRADICIONAL
                request.addProperty("name", "123456");
                */

                /* METODO USANDO UN PROPERTYINFO
                //PropertyInfo propertyInfo =new PropertyInfo();
                pi=new PropertyInfo();
                pi.setName(e.getKey().toString());
                pi.setValue(e.getValue().toString());
                pi.setType(String.class);

                request.addProperty(pi);
                */
            }

            //TODO Modelo de Sobre
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = false; // true, solo si es servicio es .NET ASMX o WCF
            envelope.setOutputSoapObject(request);

            //TODO Modelo de Transporte
            httpTransport = new HttpTransportSE(SOAP_ADDRESS, 60000); //CONEXION

            //TODO LLamada
            httpTransport.call(SOAP_ACTION, envelope); //ENVIO DE SOLICITUD

            //TODO Resultado
            response = (SoapPrimitive) envelope.getResponse();

        }catch(Exception e){
            Log.e("ERROR: ",e.getMessage());
        }finally {
            try {
                httpTransport.getServiceConnection().disconnect();
            } catch (IOException e) {
                Log.e("ERROR: ",e.getMessage());
            }
        }

        return response.toString();
    }


}
