package pe.edu.torres.alex.prototipoksoap.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import pe.edu.torres.alex.prototipoksoap.R;
import pe.edu.torres.alex.prototipoksoap.bean.AlertaBean;

public class AlertaAdapter extends ArrayAdapter<AlertaBean> {
    List<AlertaBean> data;
    int resource;
    public AlertaAdapter(@NonNull Context context, int resource, @NonNull List<AlertaBean> data) {
        super(context, resource, data);
        this.data =data;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(resource,parent,false);
        }

        ((TextView)convertView.findViewById(R.id.ModTxtTitulo)).setText(data.get(position).getAle_titulo());
        ((TextView)convertView.findViewById(R.id.ModTxtObserv)).setText(data.get(position).getAle_observ());

        return convertView;
    }

}
