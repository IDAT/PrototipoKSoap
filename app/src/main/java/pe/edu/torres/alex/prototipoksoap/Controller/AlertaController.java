package pe.edu.torres.alex.prototipoksoap.Controller;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import pe.edu.torres.alex.prototipoksoap.Services.CallbackGeneric;
import pe.edu.torres.alex.prototipoksoap.Services.HiloGeneric;
import pe.edu.torres.alex.prototipoksoap.Services.SoapClient;
import pe.edu.torres.alex.prototipoksoap.bean.AlertaBean;

public class AlertaController{

    //DECLARACIONES
    final Context context;
    public String OPERATION_NAME;
    public final String WSDL_TARGET_NAMESPACE = "http://ws.sos.idat.edu.pe/"; //http://tempuri.org/";
    public String SOAP_ADDRESS =  "http://10.0.2.2:8080/WebAlertas/AlertasWS?WSDL"; //"http://192.168.2.31/IdatService.asmx";

    public String strResult = null;
    public JSONObject jsonObject = null;

    public AlertaController(Context ctx) {
        this.context = ctx;
        this.jsonObject = new JSONObject();

    }
    public JSONObject selAlertasAll(HashMap PARAMETERS){
        try {
            OPERATION_NAME = "SelAlertaAll";
            SoapClient soapClient = new SoapClient();
            strResult = soapClient.Call(WSDL_TARGET_NAMESPACE, SOAP_ADDRESS, OPERATION_NAME, PARAMETERS);
            jsonObject = new JSONObject(strResult);
        }catch (Exception e){
            Log.e("ERROR",e.getMessage());
            try {
                jsonObject.put("status", false);
                jsonObject.put("message", e.getMessage());
            }catch(JSONException ex){
                Log.e("ERROR",ex.getMessage());
            }
        }
        return jsonObject;
    }

    public JSONObject delAlerta(HashMap PARAMETERS){
        try {
            OPERATION_NAME = "DelAlertaAll";
            SoapClient soapClient = new SoapClient();
            strResult = soapClient.Call(WSDL_TARGET_NAMESPACE, SOAP_ADDRESS, OPERATION_NAME, PARAMETERS);
            jsonObject = new JSONObject(strResult);
        }catch (Exception e){
            Log.e("ERROR",e.getMessage());
            try {
                jsonObject.put("status", false);
                jsonObject.put("message", e.getMessage());
            }catch(JSONException ex){
                Log.e("ERROR",ex.getMessage());
            }
        }
        return jsonObject;
    }

    public JSONObject updAlerta(HashMap PARAMETERS){
        try {
            OPERATION_NAME = "UpdAlerta";
            SoapClient soapClient = new SoapClient();
            strResult = soapClient.Call(WSDL_TARGET_NAMESPACE, SOAP_ADDRESS, OPERATION_NAME, PARAMETERS);
            jsonObject = new JSONObject(strResult);
        }catch (Exception e){
            Log.e("ERROR",e.getMessage());
            try {
                jsonObject.put("status", false);
                jsonObject.put("message", e.getMessage());
            }catch(JSONException ex){
                Log.e("ERROR",ex.getMessage());
            }
        }
        return jsonObject;
    }

    public JSONObject insAlerta(HashMap PARAMETERS){
        try {
            OPERATION_NAME = "InsAlerta";
            SoapClient soapClient = new SoapClient();
            strResult = soapClient.Call(WSDL_TARGET_NAMESPACE, SOAP_ADDRESS, OPERATION_NAME, PARAMETERS);
            jsonObject = new JSONObject(strResult);
        }catch (Exception e){
            Log.e("ERROR",e.getMessage());
            try {
                jsonObject.put("status", false);
                jsonObject.put("message", e.getMessage());
            }catch(JSONException ex){
                Log.e("ERROR",ex.getMessage());
            }
        }
        return jsonObject;
    }

    public String selAlertasAll2(HashMap PARAMETERS){
        ArrayList<AlertaBean> lst = new ArrayList<>();

        HiloGeneric hiloGeneric = new HiloGeneric(context, WSDL_TARGET_NAMESPACE, SOAP_ADDRESS, OPERATION_NAME, PARAMETERS, new CallbackGeneric() {
            @Override
            public void onSuccess(Object object) {
                Log.i("IDAT 2 ",object.toString());
                strResult = object.toString();
            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        hiloGeneric.execute();
        return strResult;
    }


}
