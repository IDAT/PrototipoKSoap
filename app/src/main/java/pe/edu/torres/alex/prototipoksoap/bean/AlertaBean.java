package pe.edu.torres.alex.prototipoksoap.bean;

import java.io.Serializable;
import java.util.Date;

import pe.edu.torres.alex.prototipoksoap.model.AlertaModel;

public class AlertaBean extends AlertaModel implements Serializable {
    private String ale_estdes;

    public AlertaBean() {
    }

    public AlertaBean(String ale_corel, Date ale_fecreg, String ale_tipo, String ale_titulo, String ale_observ, int ale_estcod, String ale_estdes) {
        super(ale_corel, ale_fecreg, ale_tipo, ale_titulo, ale_observ, ale_estcod);
        this.ale_estdes = ale_estdes;
    }


}
