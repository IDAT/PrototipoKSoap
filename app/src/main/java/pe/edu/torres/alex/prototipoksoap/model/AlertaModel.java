package pe.edu.torres.alex.prototipoksoap.model;

import java.util.Date;

public abstract class AlertaModel {
    private String ale_corel;
    private Date ale_fecreg;
    private String ale_tipo;
    private String ale_titulo;
    private String ale_observ;
    private int ale_estcod;

    public AlertaModel() {
    }

    public AlertaModel(String ale_corel, Date ale_fecreg, String ale_tipo, String ale_titulo, String ale_observ, int ale_estcod) {
        this.ale_corel = ale_corel;
        this.ale_fecreg = ale_fecreg;
        this.ale_tipo = ale_tipo;
        this.ale_titulo = ale_titulo;
        this.ale_observ = ale_observ;
        this.ale_estcod = ale_estcod;
    }

    public String getAle_corel() {
        return ale_corel;
    }

    public void setAle_corel(String ale_corel) {
        this.ale_corel = ale_corel;
    }

    public Date getAle_fecreg() {
        return ale_fecreg;
    }

    public void setAle_fecreg(Date ale_fecreg) {
        this.ale_fecreg = ale_fecreg;
    }

    public String getAle_tipo() {
        return ale_tipo;
    }

    public void setAle_tipo(String ale_tipo) {
        this.ale_tipo = ale_tipo;
    }

    public String getAle_titulo() {
        return ale_titulo;
    }

    public void setAle_titulo(String ale_titulo) {
        this.ale_titulo = ale_titulo;
    }

    public String getAle_observ() {
        return ale_observ;
    }

    public void setAle_observ(String ale_observ) {
        this.ale_observ = ale_observ;
    }

    public int getAle_estcod() {
        return ale_estcod;
    }

    public void setAle_estcod(int ale_estcod) {
        this.ale_estcod = ale_estcod;
    }
}
