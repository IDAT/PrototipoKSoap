package pe.edu.torres.alex.prototipoksoap;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import pe.edu.torres.alex.prototipoksoap.Adapters.AlertaAdapter;
import pe.edu.torres.alex.prototipoksoap.Controller.AlertaController;
import pe.edu.torres.alex.prototipoksoap.bean.AlertaBean;

public class MainActivity extends AppCompatActivity {

    //DECLARACIONES
    //- https://www.ibm.com/developerworks/library/ws-android/index.html
    ListView lv;
    AlertaController alertaController = new AlertaController(MainActivity.this);
    View coordinatorLayoutView;
    AlertaAdapter alertaAdapter;
    ArrayList<AlertaBean> aryAlertas = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ENLAZAR OBJECT XML A OBJECT JAVA
        coordinatorLayoutView = findViewById(R.id.MyCoordinatorLayout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO - LLAMAR A LA TAREA ASYNCTASK
                new HiloSoapClient().execute();
                /*
                alertaController= new AlertaController(MainActivity.this);
                HashMap<String, Object> mapParametros = new HashMap<>();
                mapParametros.put("arg_anio","2018");
                mapParametros.put("arg_estcod","1");
                String s = alertaController.selAlertasAll(mapParametros);
                */
            }
        });
        //ENLAZAR LISTVIEW XML A LISTVIEW JAVA
        lv = findViewById(R.id.lsvAlertas);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertaBean a = alertaAdapter.getItem(position);

                Intent x= new Intent(MainActivity.this,AlertaActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("correl",a.getAle_corel());
                bundle.putString("tipo",a.getAle_tipo());
                bundle.putString("titulo",a.getAle_titulo());
                bundle.putString("observ",a.getAle_observ());
                bundle.putInt("estcod",a.getAle_estcod());
                x.putExtras(bundle);
                startActivity(x);


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_nuevo) {
            Intent x = new Intent(MainActivity.this,AlertaActivity.class);
            startActivity(x);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //TODO - LLAMAR A LA TAREA ASYNCTASK
        new HiloSoapClient().execute();
    }

    //CLASE ASYNCTASK
    public class HiloSoapClient extends AsyncTask<String,Integer,JSONObject>{
        //DIALOG
        ProgressDialog dialog = null;

        @Override
        protected void onPreExecute() {
            //TODO - MOSTRAR CUADRO DE DIALOGO EMERGENTE
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setTitle("Aviso");
            dialog.setMessage("Cargando Alertas");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            //TODO - CONSUMO DEL SERVICIO SOAP + PARAMETROS
            HashMap<String, Object> mapParametros = new HashMap<>();
            mapParametros.put("arg_anio","2018");
            mapParametros.put("arg_estcod","1");
            return alertaController.selAlertasAll(mapParametros);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            //TODO - CERRAR PROGRESSDIALOG SI ESTA VISIBLE
            if(this.dialog.isShowing()){this.dialog.dismiss();}
            try {
                //TODO - MOSTRAR KEY MESSAGE
                Snackbar.make(coordinatorLayoutView, jsonObject.getString("message") ,Snackbar.LENGTH_LONG).show();

                if(jsonObject.getBoolean("status")) {
                    //TODO - OBTENER JSONARRAY DEL JSONOBJECT
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    aryAlertas = new ArrayList<>();

                    for (int x = 0; x < jsonArray.length(); x++) {

                        //TODO - OBTENER JSONOBJECT DEL JSONARRAY
                        JSONObject j = jsonArray.getJSONObject(x);

                        AlertaBean a = new AlertaBean();
                        a.setAle_corel(j.getString("ale_correl"));
                        a.setAle_titulo(j.getString("ale_titulo"));
                        a.setAle_observ(j.getString("ale_observ"));
                        aryAlertas.add(a);
                    }

                    alertaAdapter = new AlertaAdapter(MainActivity.this, R.layout.layout_alerta, aryAlertas);
                    alertaAdapter.notifyDataSetChanged();

                    lv.setAdapter(alertaAdapter);
                }

            }catch (Exception e){Log.e("ERROR",e.getMessage());}

        }


    }


}
