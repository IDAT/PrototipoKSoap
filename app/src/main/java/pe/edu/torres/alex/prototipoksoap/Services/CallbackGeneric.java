package pe.edu.torres.alex.prototipoksoap.Services;

public interface CallbackGeneric<T> {
    void onSuccess(T object);
    void onFailure(Exception e);
}
